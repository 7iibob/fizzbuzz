// Write a program that prints the numbers from 1 to 100. For multiples of three print “Fizz” instead of the number. For multiples of five print “Buzz” instead of the number. For numbers which are multiples of both three and five print “FizzBuzz”.

for(i = 1; i <= 100; i++){
  let out =
    i % 3 == 0 && i % 5 == 0
    ? 'FIZZBUZZ'
    : i % 3 == 0
    ? 'FIZZ'
    : i % 5 == 0
    ? 'BUZZ'
    : i

    console.log(`${i} output ${out}`);
}
